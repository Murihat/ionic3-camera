import { Component } from '@angular/core';
import { NavController, Platform, normalizeURL } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { DomSanitizer } from '@angular/platform-browser';
import { BackgroundMode } from '@ionic-native/background-mode';
import { DirectiveNormalizer } from '@angular/compiler';





@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  myImages:String;

  constructor(public navCtrl: NavController,
              private camera: Camera,
              private DomSanitizer: DomSanitizer,
              private platform: Platform,
              private backgroundMode: BackgroundMode,
              ) {

  }

  openCamera(){
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      saveToPhotoAlbum: false
    }
    
    this.camera.getPicture(options).then((imageData) => {
     this.myImages = (<any>window).Ionic.WebView.convertFileSrc(imageData);
     let base64  = 'data:image/jpeg;base64,' + imageData;

     console.log(this.myImages);
    }, (err) => {
     // Handle error
    });
  }

  openGallery(){
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      saveToPhotoAlbum: false
    }
    
    this.camera.getPicture(options).then((imageData) => {
      this.myImages = (<any>window).Ionic.WebView.convertFileSrc(imageData);
      let base64 = 'data:image/jpeg;base64,' + imageData;
      console.log(this.myImages)
      }, (err) => {
          
      
      });
  }


}